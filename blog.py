#!/usr/bin/env python

import bcrypt
import concurrent.futures
import MySQLdb
import markdown
import os.path
import re
import subprocess
import torndb
import tornado.escape
from tornado import gen
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import unicodedata

from tornado.options import define, options

try:
   define("port", group="", default=8888, help="run on the given port", type=int)
   define("listen_address", group="", default="127.0.0.1", help="run on the given listen address")
   define("mysql_host", default="", help="blog database host")
   define("mysql_database", default="bloging", help="blog database name")
   define("mysql_user", default="", help="blog database user")
   define("mysql_password", default="", help="blog database password")
except Exception:
   print "Error Database, Tidak Connect !!"
   sys.exit()

# A thread pool to be used for password hashing with bcrypt.
executor = concurrent.futures.ThreadPoolExecutor(2)


class Application(tornado.web.Application):
    def __init__(self):
           handlers = [
               (r"/", HomeHandler),
               (r"/archive", ArchiveHandler),
               (r"/feed", FeedHandler),
               (r"/golang", GoLangHandler),
               (r"/python", PythonHandler),
               (r"/ccode", CCodeHandler),
               (r"/entry/([^/]+)", EntryHandler),
               (r"/search/([^/]+)", SearchHandler),
               (r"/searchpro", SearchProHandler),
               (r"/compose", ComposeHandler),
               (r"/notfound", NotFoundHandler),
               (r"/auth/create", AuthCreateHandler),
               (r"/auth/login", AuthLoginHandler),
               (r"/auth/logout", AuthLogoutHandler),
               (r".*", HomeHandler),
           ]

           settings = dict(
               blog_title=u"Contra (Coding To Try)",
               template_path=os.path.join(os.path.dirname(__file__), "templates"),
               static_path=os.path.join(os.path.dirname(__file__), "static"),
               ui_modules={"Entry": EntryModule},
               xsrf_cookies=True,
               cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
               login_url="/auth/login",
               debug=True,
           )
           super(Application, self).__init__(handlers, **settings)

           # Have one global connection to the blog DB across all handlers

           self.db = torndb.Connection(
               host=options.mysql_host, database=options.mysql_database,
               user=options.mysql_user, password=options.mysql_password)

	   if not handlers:
            self.redirect("/",page=None)


class BaseHandler(tornado.web.RequestHandler):
    @property
    def db(self):
        return self.application.db

    def get_current_user(self):
        user_id = self.get_secure_cookie("YhgatTRfGGFStrGWU")
        if not user_id: return None
        return self.db.get("SELECT * FROM authors WHERE id = %s", int(user_id))

    def any_author_exists(self):
        return bool(self.db.get("SELECT * FROM authors LIMIT 1"))


class HomeHandler(BaseHandler):
    def get(self):
        entries = self.db.query("SELECT * FROM entries ORDER BY published DESC LIMIT 9")
        if not entries:
            self.redirect("/compose")
            return
        self.render("home.html", entries=entries)

class EntryHandler(BaseHandler):
    def get(self, slug):
        if slug!='':
           entry = self.db.get("SELECT * FROM entries WHERE slug = %s", slug)
           if not entry: self.redirect("/notfound")
           self.render("entry.html", entry=entry)
        else:
           self.redirect("/")

class SearchProHandler(tornado.web.RequestHandler):
    try:
       def post(self):
           getting = self.get_argument("search")
           self.redirect("/search/" + getting)
    except Exception:
       self.redirect("/")

class SearchHandler(BaseHandler):
    def get(self, slug):
        try:
           if slug!='':
              entry = self.db.get("SELECT * FROM entries WHERE slug like %s", slug+'%')
              entriesarchive = self.db.query("SELECT * FROM entries ORDER BY published "
                                         "DESC")
              if not entry: self.redirect("/notfound")
              self.render("search.html", entry=entry,entriesarchive=entriesarchive)
           else:
              self.redirect("/")
        except Exception:
              self.redirect("/")

class GoLangHandler(BaseHandler):
    def get(self):
        entries = self.db.query("SELECT * FROM entries WHERE category = 'golang' ORDER BY published "
                                "DESC")
        entriesarchive = self.db.query("SELECT * FROM entries ORDER BY published "
                                "DESC LIMIT 10")
        if not entries: self.redirect("/notfound")
        self.render("archive.html", entries=entries,entriesarchive=entriesarchive)

class PythonHandler(BaseHandler):
    def get(self):
        entries = self.db.query("SELECT * FROM entries WHERE category = 'python' ORDER BY published "
                                "DESC")
        entriesarchive = self.db.query("SELECT * FROM entries ORDER BY published "
                                "DESC LIMIT 10")
        if not entries: self.redirect("/notfound")
        self.render("archive.html", entries=entries,entriesarchive=entriesarchive)

class CCodeHandler(BaseHandler):
    def get(self):
        entries = self.db.query("SELECT * FROM entries WHERE category = 'c-code' ORDER BY published "
                                "DESC")
        entriesarchive = self.db.query("SELECT * FROM entries ORDER BY published "
                                "DESC LIMIT 10")
        if not entries: self.redirect("/notfound")
        self.render("archive.html", entries=entries,entriesarchive=entriesarchive)

class NotFoundHandler(BaseHandler):
    def get(self):
        entriesarchive = self.db.query("SELECT * FROM entries ORDER BY published "
                                       "DESC")
        self.render("notfound.html", entriesarchive=entriesarchive)


class ArchiveHandler(BaseHandler):
    def get(self):
        entries = self.db.query("SELECT * FROM entries ORDER BY published "
                                "DESC")
        entriesarchive = self.db.query("SELECT * FROM entries ORDER BY published "
                                "DESC LIMIT 10")
        self.render("archive.html", entries=entries,entriesarchive=entriesarchive)


class FeedHandler(BaseHandler):
    def get(self):
        entries = self.db.query("SELECT * FROM entries ORDER BY published "
                                "DESC LIMIT 10")
        self.set_header("Content-Type", "application/atom+xml")
        self.render("feed.xml", entries=entries)


class ComposeHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        id = self.get_argument("id", None)
        entry = None
        if id:
            entry = self.db.get("SELECT * FROM entries WHERE id = %s", int(id))
        self.render("compose.html", entry=entry)

    @tornado.web.authenticated
    def post(self):
        try:
           id = self.get_argument("id", None)
           title = self.get_argument("title")
           text = self.get_argument("markdown")
           html = markdown.markdown(text)
           category = self.get_argument("category")
           if id:
              entry = self.db.get("SELECT * FROM entries WHERE id = %s", int(id))
              if not entry: self.redirect("/")
              slug = entry.slug
              self.db.execute(
                  "UPDATE entries SET title = %s, markdown = %s, html = %s, category = %s"
                  "WHERE id = %s", title, text, html, category, int(id))
           else:
              slug = unicodedata.normalize("NFKD", title).encode(
                  "ascii", "ignore")
              slug = re.sub(r"[^\w]+", " ", slug)
              slug = "-".join(slug.lower().strip().split())
              if not slug: slug = "entry"
              while True:
                  e = self.db.get("SELECT * FROM entries WHERE slug = %s", slug)
                  if not e: break
                  slug += "-2"
              self.db.execute(
                  "INSERT INTO entries (author_id,title,slug,markdown,html,"
                  "published,category) VALUES (%s,%s,%s,%s,%s,UTC_TIMESTAMP(),%s)",
                  self.current_user.id, title, slug, text, html, category)
           self.redirect("/entry/" + slug)
        except Exception:
	        self.redirect("")

class AuthCreateHandler(BaseHandler):
    def get(self):
        self.render("create_author.html")

    @gen.coroutine
    def post(self):
        try:
            if self.any_author_exists():
                raise tornado.web.HTTPError(400, "author already created")
            hashed_password = yield executor.submit(
                bcrypt.hashpw, tornado.escape.utf8(self.get_argument("password")),
                bcrypt.gensalt())

            author_id = self.db.execute(
                "INSERT INTO authors (email, name, hashed_password) "
                "VALUES (%s, %s, %s)",
                self.get_argument("email"), self.get_argument("name"),
                hashed_password)
            self.set_secure_cookie("YhgatTRfGGFStrGWU", str(author_id))
            self.redirect(self.get_argument("next", "/"))
        except Exception:
           self.redirect("/")

class AuthLoginHandler(BaseHandler):
    def get(self):
        try:
           # If there are no authors, redirect to the account creation page.
           if not self.any_author_exists():
               self.redirect("/auth/create")
           else:
               self.render("login.html", error=None)
        except Exception:
           self.redirect("/")

    @gen.coroutine
    def post(self):
        try:
           author = self.db.get("SELECT * FROM authors WHERE email = %s",self.get_argument("email"))
           if not author:
               self.render("login.html", error="email not found")
               return

           hashed_password = yield executor.submit(bcrypt.hashpw, tornado.escape.utf8(self.get_argument("password")),tornado.escape.utf8(author.hashed_password))
           if hashed_password == author.hashed_password:
              #if self.get_argument("password") == author.hashed_password:
              self.set_secure_cookie("YhgatTRfGGFStrGWU", str(author.id))
              self.redirect(self.get_argument("next", "/"))
           else:
              self.render("login.html", error="incorrect password")
        except Exception:
           self.redirect("/")


class AuthLogoutHandler(BaseHandler):
    def get(self):
        self.clear_cookie("K3r3n3uy")
        self.redirect(self.get_argument("next", "/"))

class EntryModule(tornado.web.UIModule):
    def render(self, entry):
        return self.render_string("modules/entry.html", entry=entry)



def main():
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port,address=options.listen_address)
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()
