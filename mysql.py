#!/usr/bin/env python
from __future__ import print_function

from tornado import ioloop, gen
import tornado_mysql

@gen.coroutine
def main():
    conn = yield tornado_mysql.connect(host='127.0.0.1', port=, user='', passwd='', db='bloging')
    cur = conn.cursor()
    yield cur.execute("select * from entries")
    print(cur.description)
    for row in cur:
       print(row)
    cur.close()
    conn.close()
    print ("Connect Databases")

ioloop.IOLoop.current().run_sync(main)
